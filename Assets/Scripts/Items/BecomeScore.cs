﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Power.Score;
using UnityEngine;

namespace Power.Items {
public class BecomeScore : MonoBehaviour {
    private Stars _Stars;

    private void Awake() {
        _Stars = FindObjectOfType<Stars>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (!other.CompareTag("Projectile"))
            return;
        _Stars.CreateStarAt(transform.position, 1);
    }
}
}