﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Power.Items {
public class RandomizePosition : MonoBehaviour {
    [SerializeField] private Vector2 _randomizeBounds;

    private Vector2 _defaultPosition;

    private void Awake() {
        _defaultPosition = transform.localPosition;
    }

    private void OnEnable() {
        transform.localPosition = _defaultPosition +
                                  new Vector2(Random.Range(-_randomizeBounds.x, _randomizeBounds.x),
                                      Random.Range(-_randomizeBounds.y, _randomizeBounds.y));
    }

    private void OnDisable() {
        transform.localPosition = _defaultPosition;
    }
}
}