﻿using System.Collections;
using System.Collections.Generic;
using FA.Architecture.Scriptable;
using UnityEngine;

namespace Power.Score {
public class ScoreCounter : MonoBehaviour {
    [SerializeField] private int _amount = 1;

    [SerializeField] private ScriptableInt _scriptableInt;

    public void DoCount() {
        _scriptableInt.Value += _amount;
    }
}
}