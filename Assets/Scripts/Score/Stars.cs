﻿using System;
using System.Collections;
using System.Collections.Generic;
using Power.CannonBall;
using UltEvents;
using UnityEngine;

namespace Power.Score {
public class Stars : MonoBehaviour {
    [SerializeField] private Pool _pool;

    public void CreateStarAt(Vector2 position, int amount) {
        var item = _pool.Spawn();
        item.transform.position = position;
    }
}
}