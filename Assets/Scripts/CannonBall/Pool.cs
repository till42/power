﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using FA.Primitives;
using UltEvents;
using UnityEngine;

namespace Power.CannonBall {
public delegate void RecycleDelegate(GameObject go);


public class Pool : MonoBehaviour {
    [SerializeField] private GameObject _prefab;

    [SerializeField] private int _initialSize = 3;

    [SerializeField] private List<GameObject> _activeItems;

    [SerializeField] private List<GameObject> _trashedItems;

    private void Start() {
        InitPool(_initialSize);
    }

    private void InitPool(int poolSize) {
        for (int i = 0; i < poolSize; i++) {
            CreateItem();
        }
    }

    private void CreateItem() {
        var item = Instantiate(_prefab, transform);
        item.SetActive(false);
        _trashedItems.Add(item);
    }

    public void Spawn(int numberOfItems) {
        if (numberOfItems <= 0) {
            Debug.LogWarning($"Expected at least 1 item to spawn. But actual numberOfItems is {numberOfItems}");
            return;
        }

        numberOfItems.Times(SpawnAndForget);
    }

    private void SpawnAndForget() {
        Spawn();
    }

    public GameObject Spawn() {
        if (_trashedItems.Count <= 0)
            CreateItem();

        var item = _trashedItems[0];
        _trashedItems.RemoveAt(0);

        var lifeCycleEvents = item.GetComponent<LifeCycleEvents>();
        if (lifeCycleEvents == null)
            lifeCycleEvents = item.AddComponent<LifeCycleEvents>();
        lifeCycleEvents.DisableEvent.AddPersistentCall((Action<GameObject>) Trash)
            .SetArguments(item);

        item.SetActive(true);
        _activeItems.Add(item);
        return item;
    }

    private void Trash(GameObject item) {
        if (!_trashedItems.Contains(item))
            _trashedItems.Add(item);
        _activeItems.Remove(item);
    }
}
}