﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Power.CannonBall {
public class Shooting : MonoBehaviour {
    [SerializeField] private Vector2 _direction;
    [SerializeField] private float _speed;

    private Transform _transform;
    private Vector2 _defaultPosition;
    private Vector2 _destination;

    private void OnEnable() {
        _transform = transform;

        _defaultPosition = _transform.position;
        _destination = _defaultPosition + _direction;
        _transform.DOMove(_destination, _speed).SetEase(Ease.Linear).OnComplete(() => gameObject.SetActive(false));
    }

    private void OnDisable() {
        _transform.DOKill();
        _transform.position = _defaultPosition;
    }
}
}