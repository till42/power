﻿using System;
using System.Collections;
using System.Collections.Generic;
using FA.Primitives;
using UltEvents;
using UnityEngine;

namespace Power.CannonBall {
[Serializable]
public sealed class UltEventInt : UltEvent<int> {
}

public class Magazin : MonoBehaviour {
    [SerializeField] private int _maxBullets = 3;

    public int Bullets {
        get => _bullets;
        private set {
            value = Mathf.Clamp(value, 0, _maxBullets);

            if (_bullets == value)
                return;

            _bullets = value;
            _onUpdateBullets?.Invoke(value);
        }
    }

    private int _bullets;

    [SerializeField] private UltEventInt _onUpdateBullets;

    [SerializeField] private Pool _pool;

    private void Start() {
        _bullets = _maxBullets;
    }

    public void Fire() {
        if (Bullets <= 0)
            return;

        Bullets--;
        _pool.Spawn();
    }

    public void IncreaseBullets() {
        _maxBullets++;
    }

    public void Recharge(int amount) => amount.Times(RechargeOnce);
    public void RechargeOnce() => Bullets++;
}
}