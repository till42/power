﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace Power.CannonBall {
public class DisplaySprite : MonoBehaviour {
    [SerializeField] private List<Sprite> _sprites;

    private SpriteRenderer _spriteRenderer;

    private void Awake() {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void HandleUpdate(int number) {
        if (number < 0 || number >= _sprites.Count)
            return;
        _spriteRenderer.sprite = _sprites[number];
    }
}
}