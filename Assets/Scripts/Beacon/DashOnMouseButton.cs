﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Power.Beacon {
public class DashOnMouseButton : MonoBehaviour {
    [SerializeField] private float _maxDuration = 0.5f;

    [SerializeField] private MoveTowards _moveTowards;

    private bool _isDashing = false;

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            StopDashCoroutine();
            StartCoroutine(Co_Dashing());
        }

        if (Input.GetMouseButtonUp(0)) {
            StopDashCoroutine();
            DashOver();
        }
    }

    private void StopDashCoroutine() {
        if (_isDashing)
            StopAllCoroutines();
    }

    private IEnumerator Co_Dashing() {
        _isDashing = true;
        DashStart();
        yield return new WaitForSeconds(_maxDuration);
        DashOver();
        _isDashing = false;
    }

    private void DashStart() {
        _moveTowards.SetDashSpeed();
    }

    private void DashOver() {
        _moveTowards.SetDefaultSpeed();
    }
}
}