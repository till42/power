﻿using System.Collections;
using System.Collections.Generic;
using UltEvents;
using UnityEngine;

namespace Power.Beacon {
public class GivingPower : MonoBehaviour {
    [SerializeField] private UltEvent _onGivingPowerOn;

    [SerializeField] private UltEvent _onGivingPowerOff;

    public void GivingPowerOn() {
        _onGivingPowerOn?.Invoke();
    }

    public void GivingPowerOff() {
        _onGivingPowerOff?.Invoke();
    }
}
}