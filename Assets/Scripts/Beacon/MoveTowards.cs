﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Power.Beacon {
public class MoveTowards : MonoBehaviour {
    [SerializeField] private float _defaultSpeed = 25f;
    [SerializeField] private float _dashSpeed = 50f;

    [SerializeField] private Transform _object;

    private Transform _transform;
    private Vector3 _currentPosition;

    private float _currentSpeed;

    private void Awake() {
        _transform = transform;
        _currentSpeed = _defaultSpeed;
    }

    private void Update() {
        _currentPosition = _transform.position;
        _currentPosition.y = Mathf.MoveTowards(_currentPosition.y, _object.position.y, Time.deltaTime * _currentSpeed);
        _transform.position = _currentPosition;
    }

    public void SetDashSpeed() {
        _currentSpeed = _dashSpeed;
    }

    public void SetDefaultSpeed() {
        _currentSpeed = _defaultSpeed;
    }
}
}