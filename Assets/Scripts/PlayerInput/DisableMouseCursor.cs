﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Power.PlayerInput {
public class DisableMouseCursor : MonoBehaviour {
    void OnApplicationFocus(bool isFocussed) {
        Cursor.visible = false;
    }
}
}