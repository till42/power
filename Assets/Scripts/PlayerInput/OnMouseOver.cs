﻿using System.Collections;
using System.Collections.Generic;
using UltEvents;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Power.PlayerInput {
public class OnMouseOver : MonoBehaviour, IPointerEnterHandler {
    [SerializeField] private UltEvent _onMouseOver;

    public void OnPointerEnter(PointerEventData eventData) {
        _onMouseOver?.Invoke();
    }
}
}