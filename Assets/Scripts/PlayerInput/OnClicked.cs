﻿using System.Collections;
using System.Collections.Generic;
using UltEvents;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Power.PlayerInput {
public class OnClicked : MonoBehaviour, IPointerClickHandler {
    [SerializeField] private UltEvent _onClicked;

    public void OnPointerClick(PointerEventData eventData) {
        _onClicked?.Invoke();
    }
}
}