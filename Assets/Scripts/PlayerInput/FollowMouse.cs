﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Power.PlayerInput {
public class FollowMouse : MonoBehaviour {
    private Vector3 _defaultPosition;

    private Transform _transform;

    private float _minY;
    private float _maxY;
    private Camera _cam;

    private void Awake() {
        _transform = transform;
        _defaultPosition = _transform.position;
        _cam = Camera.main;

        _minY = 0f;
        _maxY = 200f;
    }

    void Update() {
        // if (Input.GetButtonDown("Fire1")) {
        // Construct a ray from the current mouse coordinates
        var pos = _cam.ScreenToWorldPoint(Input.mousePosition);
        pos.z = _defaultPosition.z;
        // pos.x = _defaultPosition.x;
        pos.y = Mathf.Clamp(pos.y, _minY, _maxY);
        transform.position = pos;
        // }
    }
}
}