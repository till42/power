﻿using System.Collections;
using System.Collections.Generic;
using UltEvents;
using UnityEngine;

namespace Power.Upgrades {
public class Purchaseable : MonoBehaviour {
    [SerializeField] private UpgradeTierDefinition _upgradeTierDefinition;
    public UpgradeTierDefinition UpgradeTierDefinition => _upgradeTierDefinition;

    [SerializeField] public UltEvent OnUpdate;

    public void SetUpgradeTierDefinition(UpgradeTierDefinition upgradeTierDefinition) {
        _upgradeTierDefinition = upgradeTierDefinition;
        if (_upgradeTierDefinition != null)
            gameObject.SetActive(true);
        else
            gameObject.SetActive(false);

        OnUpdate?.Invoke();
    }

    public void Purchase() {
        if (_upgradeTierDefinition == null)
            return;
        _upgradeTierDefinition.Purchase();
    }
}
}