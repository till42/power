﻿using System;
using System.Collections;
using System.Collections.Generic;
using FA.Architecture.Scriptable;
using Power.CannonBall;
using UltEvents;
using UnityEngine;

namespace Power.Upgrades {
[Serializable]
public class UpgradeTierDefinition : MonoBehaviour {
    [SerializeField] private int _price;
    public int Price => _price;

    [SerializeField] private int _tier = 0;
    public int Tier => _tier;

    [SerializeField] private bool _isPurchased = false;

    [SerializeField] private ScriptableInt _score;

    [SerializeField] private UltEventInt _onPurchased;

    public bool IsPurchaseable => _score >= _price && !_isPurchased;

    public void Purchase() {
        if (!IsPurchaseable)
            return;

        _isPurchased = true;
        _onPurchased?.Invoke(Tier);
        _score.Value -= _price;
    }
}
}