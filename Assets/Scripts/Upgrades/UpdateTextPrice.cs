﻿using System;
using System.Collections;
using System.Collections.Generic;
using Power.Upgrades;
using UnityEngine;
using TMPro;


public class UpdateTextPrice : MonoBehaviour {
    private TMP_Text _tmpText;

    [SerializeField] private Purchaseable _purchaseable;

    private void OnEnable() {
        Refresh();
    }

    public void Refresh() {
        if (_tmpText == null)
            _tmpText = GetComponent<TMP_Text>();
        var upgradeTierDefinition = _purchaseable.UpgradeTierDefinition;
        if (upgradeTierDefinition == null) {
            _tmpText.text = "";
            return;
        }

        _tmpText.text = upgradeTierDefinition.Price.ToString();
    }
}