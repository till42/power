﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;

namespace Power.Upgrades {
public class Upgradeable : MonoBehaviour {
    [SerializeField] private int _tier = 0;
    public int Tier => _tier;

    [SerializeField] private List<GameObject> TierDisplayGameObjects;
    [SerializeField] private List<UpgradeTierDefinition> _upgradeTierDefinitions;
    [SerializeField] private Purchaseable _purchaseable;

    private void Start() {
        Refresh();
    }

    public void SetTier(int tier) {
        _tier = tier;
        Refresh();
    }

    [Button]
    public void Refresh() {
        for (int i = 0; i < TierDisplayGameObjects.Count; i++) {
            if (i < _tier)
                TierDisplayGameObjects[i].SetActive(true);
            else
                TierDisplayGameObjects[i].SetActive(false);
        }

        var upgradeTierDefinition = GetPurchaseable();
        if (upgradeTierDefinition != null)
            _purchaseable.SetUpgradeTierDefinition(upgradeTierDefinition);
        else
            _purchaseable.SetUpgradeTierDefinition(null);
    }

    private UpgradeTierDefinition GetPurchaseable() {
        for (int i = 0; i < _upgradeTierDefinitions.Count; i++) {
            if (_upgradeTierDefinitions[i].IsPurchaseable)
                return _upgradeTierDefinitions[i];
        }

        return null;
    }
}
}