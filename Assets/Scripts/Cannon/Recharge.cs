﻿using System.Collections;
using System.Collections.Generic;
using Power.Beacon;
using Power.CannonBall;
using UnityEngine;

namespace Power.Cannon {
public class Recharge : MonoBehaviour {
    [SerializeField] private Magazin _magazin;

    [SerializeField] private float _rechargeRate = 5f;

    public void RechargeOn(Collider2D recharger) {
        var givingPower = recharger.GetComponent<GivingPower>();
        if (givingPower == null)
            return;

        givingPower.GivingPowerOn();
        StartCoroutine(Co_Recharging());
    }

    public void RechargeOff(Collider2D recharger) {
        var givingPower = recharger.GetComponent<GivingPower>();
        if (givingPower == null)
            return;

        givingPower.GivingPowerOff();

        StopAllCoroutines();
    }

    private IEnumerator Co_Recharging() {
        while (true) {
            yield return new WaitForSeconds(_rechargeRate);
            _magazin.RechargeOnce();
        }
    }
}
}