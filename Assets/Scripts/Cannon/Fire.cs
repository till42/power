﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Power.CannonBall;
using UltEvents;
using UnityEngine;

namespace Power.Cannon {
public class Fire : MonoBehaviour {
    [SerializeField] private float _rateOfFire = 2f;

    [SerializeField] private UltEvent _onFire;

    private void Start() {
        StartCoroutine(Co_AutoFire());
    }

    public void IncreaseRate() {
        _rateOfFire *= 0.75f;
    }

    private IEnumerator Co_AutoFire() {
        while (true) {
            yield return new WaitForSeconds(_rateOfFire);
            _onFire?.Invoke();
        }
    }
}
}