﻿using UnityEngine;
using System;

namespace FA.Architecture.Scriptable {
[CreateAssetMenu(menuName = "FantasyArts/ScriptableInt")]
public class ScriptableInt : ScriptableT<int> {
    public void Add(int amount) {
        Value += amount;
    }

    public static implicit operator int(ScriptableInt scriptableInt) => scriptableInt.Value;
    
    
}
}