﻿using System;
using System.Collections;
using System.Collections.Generic;
using UltEvents;
using UnityEngine;

namespace FA.Architecture.Scriptable {
public abstract class ScriptableT<T> : ScriptableObject {
    [SerializeField] private T _value = default(T);

    [SerializeField] public UltEvent OnValueChanged;

    public virtual void Reset() {
        _value = default(T);
    }

    public T Value {
        get => _value;
        set {
            this._value = value;
            OnValueChanged?.Invoke();
        }
    }
}
}