﻿using System;
using System.Collections;
using System.Collections.Generic;
using UltEvents;
using UnityEngine;
using UnityEngine.Events;

namespace FA.Architecture.Scriptable {
public class ScriptableIntListener : MonoBehaviour {
    [SerializeField] private ScriptableInt _scriptableInt;

    [SerializeField] protected UltEvent _response;

    protected virtual void OnEnable() {
        _scriptableInt.OnValueChanged.AddPersistentCall((Action) RaiseEvent);
    }

    protected virtual void OnDisable() {
        _scriptableInt.OnValueChanged.RemovePersistentCall((Action) RaiseEvent);
    }

    private void RaiseEvent() {
        _response?.Invoke();
    }
}
}