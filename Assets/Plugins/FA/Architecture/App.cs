﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.Architecture {
public static class App {
    private const string AppGameObjectName = "App";

    private static GameObject _app;

    // Runs before a scene gets loaded
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void LoadApplication() {
        CreateApp();

        //let it live
        GameObject.DontDestroyOnLoad(_app);
    }

    public static void CreateApp() {
        //instantiate the application prefab (find it in the resources folder)  
        _app = GameObject.Instantiate(Resources.Load(AppGameObjectName)) as GameObject;

        //if not found: warning message and instantiate empty gameobject
        if (_app == null) {
            Debug.LogWarning(
                $"Could not find application prefab with the name {AppGameObjectName} in the prefab folder");
            _app = new GameObject();
        }

        //rename the gameobject
        _app.name = AppGameObjectName;
    }

    /// <summary>
    /// get the reference to a component (single instance) in a child of the application prefab
    /// </summary>
    /// <typeparam name="T">type of the component</typeparam>
    /// <returns>Component of the specified type in the application game object</returns>
    public static T GetReference<T>(bool includeInactive = false) where T : Component {
        if (_app != null) return _app.GetComponentInChildren<T>(includeInactive);

        Debug.LogWarning("application was not created");
        return null;
    }
}
}