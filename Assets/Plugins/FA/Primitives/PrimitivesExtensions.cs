﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.Primitives {
public static class PrimitivesExtensions {
    public static void Times(this int count, Action action) {
        for (int i = 0; i < count; i++) {
            action();
        }
    }
}
}