﻿using System;
using System.Collections;
using System.Collections.Generic;
using FA.Architecture.Scriptable;
using FA.Primitives;
using UnityEngine;
using TMPro;

namespace FA.UI {
public class UpdateTextScriptableInt : MonoBehaviour {
    [SerializeField] private ScriptableInt _scriptableInt;

    private TMP_Text _tmpText;

    private void Awake() {
        _tmpText = GetComponent<TMP_Text>();
    }

    private void Start() {
        _scriptableInt.OnValueChanged.AddPersistentCall((Action) handleUpdate);
        handleUpdate();
    }

    private void handleUpdate() {
        _tmpText.text = _scriptableInt.Value.ToString();
    }

    private void OnDisable() {
        _scriptableInt.OnValueChanged.RemovePersistentCall((Action) handleUpdate);
    }
}
}