﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.UI {
public class SetFontFilterMode : MonoBehaviour {
    [SerializeField] private Font _font;

    private void Start() {
        _font.material.mainTexture.filterMode = FilterMode.Point;
    }
}
}