var FAJavaHelper = {
	$JavaVar : {
		
	},
	
    GetURLFromPage: function () {
		var returnStr = "NO URL";	
		try{
			returnStr = (window.location != window.parent.location)
						? document.referrer : document.location.href;
		} catch(error){
				console.log("GetURLFromPage() error =" + error);
		}
		
		var bufferSize = lengthBytesUTF8(returnStr) + 1;
        var buffer = _malloc(bufferSize);
        stringToUTF8(returnStr, buffer, bufferSize);
        return buffer;	
    },
 
    GetQueryParam: function(paramId) {
        var urlParams = new URLSearchParams(location.search);
        var param = urlParams.get(Pointer_stringify(paramId));
        console.log("JavaScript read param: " + param);
        var bufferSize = lengthBytesUTF8(param) + 1;
        var buffer = _malloc(bufferSize);
        stringToUTF8(param, buffer, bufferSize);
        return buffer;
    },


	GetBrowser: function (){
		 var returnStr = "no browser detected";
		 try {
			returnStr = window.navigator.userAgent;
		
			var ua=window.navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
			 if(/trident/i.test(M[1])){
				tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
				returnStr="IE "+(tem[1]||'');
			}   
			if(M[1]==='Chrome'){
				tem=ua.match(/\bOPR|Edge\/(\d+)/);
				if(tem!=null){
					returnStr = "Opera "+ tem[1];
				}
			}
		
			M=M[2]? [M[1], M[2]]: [window.navigator.appName, window.navigator.appVersion, '-?'];
			if((tem=ua.match(/version\/(\d+)/i))!=null)
				{M.splice(1,1,tem[1]);}
    
			var returnStr = M[0]+" "+M[1];
		 } catch (error){
			console.log("GetBrowser() error: "+error);
		 }
		var bufferSize = lengthBytesUTF8(returnStr) + 1
        var buffer = _malloc(bufferSize);
        stringToUTF8(returnStr, buffer, bufferSize);
        return buffer;
	}
};

autoAddDeps(FAJavaHelper, '$JavaVar');
mergeInto(LibraryManager.library, FAJavaHelper);